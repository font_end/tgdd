
var swiper = new Swiper('.swiper-container', {
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});


$(document).ready(function () {
    $(".swiper-button-prev").hide();
    $(".swiper-button-next").hide();
    $('#slider-pro').hover(function () {
        $(".swiper-button-prev").toggle();
        $(".swiper-button-next").toggle();
    });

    // Ẩn hiện nội dung bài viết
    $('.dtctbox ').hide();
    $(".docthem").click(function () {
        $('.dtctbox').slideDown();
        $(".rutgon").css("display", "block");
        $(".docthem").hide();

    });
    
    $(".rutgon").click(function () {

        $('.dtctbox ').slideUp();
        $(".rutgon").hide();
        $(".docthem").show();
        

    });
    $('.detail-pro ').hide();
   
    $(".chitiet").click(function () {
        $('.detail-pro ').show();
        $('.scroll ').show();
      
    });
    $(".detail-pro .fas ").click(function () {
        $('.detail-pro ').hide();

    });
   

    // ẩn hiện fom send nội dung

        var number = 0;
        $(".send-dg").on('click', function() { 
            number ++;
            $( "#send" ).toggle(function(a,b){
              if(number%2 !=0){
                $(".send-dg").text('Đóng lại');
              }else{
                $(".send-dg").text('Gửi đánh giá của bạn');
              }
            });
        });
    
    
    $('.detail-pro1 ').hide();
    $(".btnsend").click(function () {
        $('.detail-pro1 ').show();
        $('.wrap_popup ').show();
    });
    $(".detail-pro1 .fas ").click(function () {
        $('.detail-pro1 ').hide();

    });

    //slide hình ảnh 
    $('.imggaraly').owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
         autoplayTimeout:2000,
      autoplayHoverPause:true,
        responsiveClass:true,
         responsive:{
            0:{
        items:2,
        nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            dots:false,
            nav: false,
            loop:true
              }
          }
        })
});

// menu
$(".title-slider ul li:nth-child(1) a").css('border-bottom','3px solid #ff001f');
    $(".title-slider ul li:nth-child(1)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.title-slider').offset().top-40 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(1) a").css('border-bottom','3px solid #ff001f')
    }); });


$(".title-slider ul li:nth-child(1)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.title-slider').offset().top-46 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(1) a").css('border-bottom','3px solid #ff001f')
    }); });

$(".title-slider ul li:nth-child(3)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.danh-gia').offset().top-60 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(3) a").css('border-bottom','3px solid #ff001f')
    }); });


     $(".title-slider ul li:nth-child(4)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.form-dg').offset().top-50 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(4) a").css('border-bottom','3px solid #ff001f')
    }); });

  

    $(".title-slider ul li:nth-child(5)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.dtbox').offset().top-40 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(5) a").css('border-bottom','3px solid #ff001f')
    }); });

     $(".title-slider ul li:nth-child(6)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.dtbox').offset().top-40 }, 1000); }); 

    $(".title-slider ul li:nth-child(7)").on('click', function() { 
    $("HTML, BODY").animate({ scrollTop: $('.new').offset().top-40 },1000,function(){
        $(".title-slider ul li a").css('border','none'),
        $(".title-slider ul li:nth-child(7) a").css('border-bottom','3px solid #ff001f')
    }); });
    
    $(window).bind("scroll", function() {
     var top = $(window).scrollTop();
 
     if (top> 1004) { //Khoảng cách đã đo được
          $(".title-slider ").addClass("title-sliderfix");
          $('.phone').css('display','inline');
          $('.addcart').css('display','inline');
     } else {
      $(".title-slider ").removeClass("title-sliderfix");
         $('.phone').css('display','none');
          $('.addcart').css('display','none');
        } 
     });
     // laod chi tiết sản phẩm
     function getDetail() {
        var url = new URL(window.location.href);
        var code = url.searchParams.get('id');
        if (code != null) {
            $.ajax({
                method: 'GET',
                url: 'https://www.jasonbase.com/things/' + code + '.json',
                success: function(res) {
                    var n =  '<h1 class="dttname">' +
                         res.name +
                    '<span class="nosku">(No.00450534)</span>'+
                    '</h1>'+
                    ' <div class="ratting">'+
                    ' <span class="fa fa-star checked"></span>'+
                    ' <span class="fa fa-star checked"></span>'+
                    ' <span class="fa fa-star checked"></span>'+
                    ' <span class="fa fa-star"></span>'+
                    '<span class="fa fa-star"></span>'+

                    '</div>'+
                    '<p class="number-rt">'+
                    '<a href="#danh-gia-nhan-xet" title="" toscroll="">371 khách hàng đánh giá</a>'+
                    '| <a href="#hoi-dap" title="" toscroll="">838 câu hỏi được trả lời</a></p>'
                    $('.name').html(n);
                    for(var i=0; i<res.image-list ;){

                    }
                    var img = '<div class="tab-pane active" id="pic-1">'+
                    '<img src="'+ res.image+'">'+
                        '</div>';
                    $('.picture').html(img);
                    var listimg = 
                    '<li class=""><a data-target="#pic-1" data-toggle="tab"aria-expanded="false">'
                    '<img src="https://cdn.tgdd.vn/Products/Images/42/147939/samsung-galaxy-s9-plus-64gb-xanh-san-ho-2-1-600x600.jpg"></a></li>'
                    $('#listimg').html(listimg);
                },
                error: function(err) {
                    console.log(err);
                    window.alert('Lỗi');    
                }
            });
        }
    }



     // load ds san phẩm

  
     function getData() {
   $.ajax({
 method: "GET", 
url: "https://www.jasonbase.com/things/mxJY.json",
success: function (res) {
    var html = "";
    for (var i = 0; i < 6; i++) {
        html += '<div class="col-sm-4 col-md-2 col-6 image">' +
            '<img src="'+ res.data[i].image +'" alt="">'+
               '<div class="product-title"><a href="./details.html?id=' + res.data[i].id + 'class="sp-custom-66-2">'+
                    res.data[i].name +
                '</a>'+
                '</div>'+
            '<div class="product-price">'+
                res.data[i].price +
            '</div>'+
        '</div>';
    }
    $('.product-type').html(html);
},
error: function (err) {
    console.log(err);
}
});
}

     $(document).ready(function (){
         getDetail();
      getData();
      })

